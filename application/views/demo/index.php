<div class="container" id="demo">
    <div class="row col-md-12 fullwidth">
        <h2><?php echo $title; ?></h2>
    </div>
    <?php $i = 0; ?>
    <?php foreach ($news as $news_item): ?>
    <?php if($i === 0): ?>
        <div class="row col-md-12">            
    <?php elseif($i % 3 !== 0):?>
    <?php else: ?>
        </div>
        <div class="row col-md-12">
    <?php endif; ?>
    <?php $i++; ?>
        <div class="row col-md-4">
            <h3><?php echo $news_item['title']; ?></h3>
            
                    <?php echo $news_item['text']; ?>
            
            <p><a href="<?php echo site_url('demo/'.$news_item['slug']); ?>">View article</a></p>
        </div>
    <?php endforeach; ?> 
    </div>
</div> 
