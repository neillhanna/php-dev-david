<main class="container" id="portfolio">
        <h1 class="page-title">Portfolio</h1>
		<div class="row col-md-12">
            <div class="row col-md-6">
                    <div class="panel panel-primary">
                            <header class="panel-heading text-center">Proof Reading</header>
                        <div class="panel-body">
                            <img class="img-responsive" data-toggle="modal" data-target="#myModal" src="<?=IMG_URL?>proofreading.png" data-urlone="<?=IMG_URL?>proofreading-responsive.png" data-urltwo="" alt="proofreading.org"/>
                            <span>URL: <a href="http://dngh.uk.nf/home.html" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Made HTML from cms and made page responsive, original site here <a href="http://proofreading.org">proofreading.org</a>
                                </p>
                            </details>
                        </div>
                </div>
            </div>
            <div class="row col-md-6">
                    <div class="panel panel-primary">
                            <header class="panel-heading text-center">Get Smart Apparel</header>
                        <div class="panel-body">
                            <img class="img-responsive" data-toggle="modal" data-target="#myModal" src="<?=IMG_URL?>getsmartapparel.png" data-urlone="<?=IMG_URL?>getsmartapparel2.png" data-urltwo="<?=IMG_URL?>getsmartapparel3.png" alt="get smart apparel, custom build suits, shirts and shoes"/>
                            <span>URL: <a href="http://getsmartapparel.com" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Wordpress plugin customization for this custom suit builder site
                                </p>
                            </details>
                        </div>
                    </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="row col-md-6">
                    <div class="panel panel-primary">
                            <header class="panel-heading text-center">Christiana Hart</header>
                        <div class="panel-body">
                            <img class="img-responsive" src="<?=IMG_URL?>christianahart1.png" alt="Christiana Hart"/>
                            <span>URL: <a href="http://christianahart.com" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Online fashionable tie store.
                                </p>
                            </details>
                        </div>
                </div>
            </div>
            <div class="row col-md-6">
                    <div class="panel panel-primary">
                            <header class="panel-heading text-center">500 PSI</header>
                        <div class="panel-body">
                            <img class="img-responsive" src="<?=IMG_URL?>500psi1.png" alt="500 psi diving supplies"/>
                            <span>URL: <a href="http://500psi.com" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Online diving supplies wholesaler.
                                </p>
                            </details>
                        </div>
                    </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="row col-md-6">
                    <div class="panel panel-primary">
                            <header class="panel-heading text-center">Ski Hall</header>
                        <div class="panel-body">
                            <img class="img-responsive" src="<?=IMG_URL?>skihall1.png" alt="Ski Hall"/>
                            <span>URL: <a href="http://skihall.com" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                The U.S. Ski &amp; Snowboard Hall of Fame and Museum.
                                </p>
                            </details>
                        </div>
                    </div>
            </div>
            <div class="row col-md-6">
                    <div class="panel panel-primary">
                            <header class="panel-heading text-center">Mary Beth Pace therapist</header>
                        <div class="panel-body">
                            <img class="img-responsive" data-toggle="modal" data-target="#myModal" src="<?=IMG_URL?>mbptherapist1.png" alt="Mary Beth Pace Therapist"  data-urlone="<?=IMG_URL?>mbp_responsive_showcase1.png" data-urltwo="<?=IMG_URL?>mbp_responsive_showcase2.png"/>
                            <span>URL: <a href="http://mbptherapist.com" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Blog and business site for a counsellor and therapist.
                                </p>
                            </details>
                        </div>
                    </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="row col-md-6">
                    <div class="panel panel-primary">
                            <header class="panel-heading text-center">Data University</header>
                        <div class="panel-body">
                            <img class="img-responsive" src="<?=IMG_URL?>datau1.png" alt="Data University"/>
                            <span>URL: <a href="http://datauniversity.com" target="_blank" >Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Tailored custom php cart where users can purchase access to online webinars on marketing, integrated paypal, restructured front end flow adding many fixes and backend functionalities.
                                </p>
                            </details>
                        </div>
                </div>
            </div>            
            <div class="row col-md-6">
                    <div class="panel panel-primary">
                            <header class="panel-heading text-center">Feline Luxury escapes</header>
                            <div class="panel-body">
                                <img class="img-responsive" data-toggle="modal" data-target="#myModal" src="<?=IMG_URL?>felineluxuryescapes1.png" alt="feline luxury escapes" data-urlone="<?=IMG_URL?>feline_responsive_showcase1.png" data-urltwo="<?=IMG_URL?>feline_responsive_showcase2.png"/>
                                <span>URL: <a href="http://feline-luxury-escapes.com/" target="_blank">Link to site</a></span>
                                <br />
                                <details>
                                <summary>About this project</summary>
                                    <p>
                                    Create pages, upload images, add galleries and slideshow with SEO tags and textual content, add header images plus set up contact page. Tested out some popular existing WordPress plugins for galleries.
                                    </p>
                                </details>
                            </div>
                    </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="row col-md-6">
                <div class="panel panel-primary">
                        <header class="panel-heading text-center text-center">Next Brides</header>
                        <div class="panel-body">
                            <img class="img-responsive" src="<?=IMG_URL?>nextbrides1.png" alt="Pass on wedding gear to the next bride, rent, sell, buy"/>
                            <span class="disabled">URL: <a href="http://nextbrides.com" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Pass on wedding dresses and other gear to the next bride, buy sell, or rent.
                                </p>
                            </details>
                        </div>
                </div>
            </div>
            <div class="row col-md-6">
                <div class="panel panel-primary">
                        <header class="panel-heading text-center text-center">Neural Index</header>
                        <div class="panel-body">
                            <img class="img-responsive" src="<?=IMG_URL?>neuralindex1.png" alt="Neural index, stock price prediction software"/>
                            <span class="disabled">URL: <a href="http://neuralindex.com" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Data driven stock price prediction software. Use highcharts and google charts software
                                </p>
                            </details>
                        </div>
                </div>
            </div>
        </div>        
        <div class="row col-md-12">
            <div class="row col-md-6">
                <div class="panel panel-primary">
                        <header class="panel-heading text-center">Promotionalism</header>
                        <div class="panel-body">
                            <img class="img-responsive" src="<?=IMG_URL?>promotionalism1.png" alt="Promotional products to buy in bulk for your business"/>
                            <span>URL: <a href="http://promotionalism.com" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Promotional products for busnesses to giveaway or sell to promote themselves.
                                </p>
                            </details>
                        </div>
                </div>
            </div>
            <div class="row col-md-6">
                <div class="panel panel-primary">
                        <header class="panel-heading text-center">Lawn Doctor</header>
                        <div class="panel-body">
                            <img class="img-responsive" src="<?=IMG_URL?>lawndoctor1.png" alt="Lawn doctor A B testing"/>
                            <span>URL: <a href="http://lawndoctor.com" target="_blank">Link to site</a></span>
                            <br />
                            <details>
                                <summary>About this project</summary>
                                <p>
                                Slice and style secondary homepage and interior form to work with existing site then serve combinations of old and new designs to test performance metrics.
                                </p>
                            </details>
                        </div>
                </div>
            </div>
        </div>        
</main>
<div class="modal" id="myModal" role="dialog">
  <div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header">
		<button class="close" type="button" data-dismiss="modal">×</button>
		<h3 class="modal-title">Gallery</h3>
	</div>
	<div class="modal-body">
		<div id="modalCarousel" class="carousel">
 
                    <div class="carousel-inner">
                    
                    </div>
                    
                    <a class="left carousel-control" href="#modalCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#modalCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>                    
                </div>
	</div>
	<div class="modal-footer">
		<button class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
   </div>
  </div>
</div>