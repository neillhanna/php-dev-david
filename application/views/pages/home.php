<main id="home" class="container ">
    <div class="row col-md-12">
        <section class=" text-center">
                <h1>Welcome to the PHP section of my website</h1>
                <p class="">I have split my website into two sections to demonstrate the use of different technologies.
                This section makes use of codeignighter as well as some free public API's.
                </p>
            <div class="row col-md-6">
                <section class="panel panel-primary">
                    <h2 class="panel-heading">Technical specifications</h2>
                    <div class="panel-body text-left">
                        <ul>
                            <li>Full LAMP stack (Linux server, Apache web server, Mysql database and PHP programming language)</li>
                            <li>Codeignighter PHP framework to speed up development and make code easier to understand</li>
                            <li>HTML5 and CSS3</li>
                            <li>Twitter bootstrap for a mobile first design approach to responsiveness</li>
                        </ul>
                    </div>
                </section>
            </div>
            <div class="row col-md-6">
                <section class="panel panel-primary">
                    <h2 class="panel-heading">Technical requirements</h2>
                    <div class="panel-body text-left">
                        <ul>
                            <li>IE8 and above and all major browsers on desktop and mobile</li>
                        </ul>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>
                </section>
            </div>
            <table class="table">
                <tr>
                    <td class="img-wrapper"><img src="<?=base_url()?>assets/images/linux.png" alt="Linux server" class="img-responsive center-block" /></td>
                    <td class="img-wrapper"><img src="<?=base_url()?>assets/images/apache.png" alt="Apache web server" class="img-responsive center-block"/></td>
                    <td class="img-wrapper"><img src="<?=base_url()?>assets/images/mysql.png" alt="Mysql database" class="img-responsive center-block"/></td>      
                    <td class="img-wrapper"><img src="<?=base_url()?>assets/images/php.png" alt="PHP programming language" class="img-responsive center-block"/></td>
                    <td class="img-wrapper"><img src="<?=base_url()?>assets/images/bootstrap.png" alt="Twitter bootstrap" class="img-responsive center-block"/></td>
                    <td class="img-wrapper"><img src="<?=base_url()?>assets/images/html5.png" alt="HTML5 markup language" class="img-responsive center-block"/></td>
                </tr>
            </table>
        </section>
    </div>
</main>

