<?php
    $isActive = array("News"=>"","Images"=>"","Portfolio"=>"");
    $isActive[$page] = " class='active' ";
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?=$page?> &lt;dev:david&gt;</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/stylesheets/magnific-popup.css" rel="stylesheet">
        <link rel='stylesheet' href="<?=base_url()?>assets/stylesheets/style.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/javascripts/jquery.magnific-popup.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <script src="<?=base_url()?>assets/javascripts/scripts.js"></script>
</head>
<body>

    <header>
            
    </header>
    
    <nav class="nav navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button  type="button" class="navbar-right btn-lg btn btn-default navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            
            <a class="navbar-brand" href="<?= base_url() ?>home">&lt;dev&#58;david &#47;&gt;</a>            

            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav ">
                    <li <?=$isActive['News']?>><a href="<?= base_url() ?>news">news</a></li>
                    <li <?=$isActive['Images']?>><a href="<?= base_url()?>images">Images</a></li>
                    <li <?=$isActive['Portfolio']?>><a href="<?= base_url()?>portfolio">Portfolio</a></li>
                    <!--<li><a href="https://dev-dngh.rhcloud.com/#/mean">mean</a></li>                    
                    <li><a href="https://dev-dngh.rhcloud.com/#/demo">demo</a></li>-->
                </ul>
                <button id="switch_site_btn" class="btn navbar-btn" onclick="location.href='https://dev-dngh.rhcloud.com'" >Switch to NodeJS site</button>
            </div>
        </div>
    </nav>
    