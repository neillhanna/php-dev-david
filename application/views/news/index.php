<div class="container" id="news">
    <section class="row col-md-12 fullwidth">
        <form id="topSearchForm" method="post" action="<?=base_url()?>news" class="formNewsSearch form-group center-block">
            <input id="topSearch" class="form-control pull-left" type="text" placeholder="Search news stories"/>
            <button class="form-control btn btn-default pull-left"><span class="glyphicon glyphicon-search"></span></button>
        </form>
        <br />
        <h2 class="page-title"><?php echo $title; ?></h2>
    </section>
    <main id="content" class="row col-md-9">
        
        <?php        
        foreach($news["results"] AS $news_item):

        if(preg_match("/youtube.com/",$news_item["iurl"])):
                $title = htmlspecialchars($news_item["title"]);
                $kwic = htmlspecialchars($news_item["kwic"]);
                $url = htmlentities($news_item["url"]);
                $iurl = htmlentities($news_item["iurl"]);
        ?>
        <div class="article-group row col-md-12">
            <article class="row col-md-12 well clearfix ">
                <iframe class="center-block" <?php if($iurl != ""){echo "src='$iurl?autoplay=0'";}?> ></iframe>
                <h4><strong><?=$title?></strong></h4>
                <div class="snippet"><?=$kwic?></div>
                <br />
                <button class="readArticleBtn btn btn-primary center-block" data-toggle="modal" data-target="#modal" data-article="<?=$url?>" data-title="<?=$title?>" >Read Article</button>
            </article>
            <?php if($news_item["related"]): ?>
                <div class="related row col-md-12 text-center">
                    <span><strong>Related articles</strong></span>
                    <ul class="text-left">
                        <?php 
                        $related_items = $news_item["related"];
                        foreach($related_items AS $ritem):
                            $title = htmlspecialchars($ritem["title"]);
                            $url = htmlentities($ritem["url"]);
                        
                        ?>
                        <li data-toggle='modal' data-target='#modal' data-article='<?=$url?>' data-title="<?=$title?>" ><?=$title?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>    
        <?php else: ?>
        <div class="article-group row col-md-12">
            <article class=" well clearfix row col-md-12">
                <?php
                $title = htmlspecialchars($news_item["title"]);
                $kwic = htmlspecialchars($news_item["kwic"]);
                $url = htmlentities($news_item["url"]);
                $iurl = htmlentities($news_item["iurl"]);
                $image = checkImageUrl($iurl, $s, $trending);
                ?>
                <img class="center-block" src="<?=$image?>" alt="" />
                <h4><strong><?= $title?></strong></h4>
                <div class="snippet"><?=$kwic?></div>
                <br />
                <button class="readArticleBtn btn btn-primary center-block" data-toggle="modal" data-target="#modal" data-article="<?=$url?>" data-title="<?=$title?>" >Read Article</button>
            </article>
            <?php if($news_item["related"]): ?>
                <div class="related row col-md-12 text-center">
                    <span><strong>Related articles</strong></span>
                    <ul class="text-left">
                        <?php 
                        $related_items = $news_item["related"];
                        foreach($related_items AS $ritem):
                            $title = htmlspecialchars($ritem["title"]);
                            $url = htmlentities($ritem["url"]);
                        
                        ?>
                        <li data-toggle='modal' data-target='#modal' data-article='<?=$url?>' data-title="<?=$title?>" ><?=$title?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
        <?php
        endif;
        endforeach;
        ?>

        </main>
        
        <!-- Side bar for Tag cloud and atribution-->
        <aside id="sidebar" class="row col-md-3">
            <div id="sidebar_inner" class="center-block">
                <div class="tag-cloud center-block">
                    <h2>Trending</h2>
                    <?php foreach($trends["trends"] AS $trend): ?>
                        <a href="<?=base_url()?>news/trending/<?=preg_replace('/ /','%20',$trend)?>"><?=$trend?></a>
                    <?php endforeach; ?>
                </div>
                <br />
                <form id="sideSearchForm" method="post" action="<?=base_url()?>news" class="formNewsSearch form-group center-block">
                    <input id="sideSearch" class="form-control pull-left" type="text" value="" placeholder="Search news stories"/>
                    <button class="form-control btn btn-default pull-left"><span class="glyphicon glyphicon-search"></span></button>
                </form>
                <br />
                <?php if($s != ""):
                ?>
                <button id="newsLatest" class="btn btn-danger center-block" onclick="location.href='<?=base_url() ?>news'">Latest News</button>
                <?php endif; ?>
                <br />
                <a href="http://www.faroo.com" target="_blank" title="FAR00 Web Search">
                <img class="center-block" src= "http://www.faroo.com/hp/api/faroo_attribution.png" 
                alt="FAROO Web Search // Full privacy // No spam." style="border:0;" /></a>
            </div>
        </aside>

        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalLabel">New message</h4>
                    </div>
                    <iframe class="modal-body">
                       
                    </iframe>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>      
        
</div>

<?php
function checkImageUrl($iurl, $s, $trending){
    if($s == "" && $iurl != ""):
    $image = (@getImageSize($iurl) ? $iurl : base_url()."assets/images/latest.png");
    elseif($iurl != "" && isset($news[0]["related"]) && !$trending):
    $image = (@getImageSize($iurl) ? $iurl : base_url()."assets/images/archive.png");
    elseif($iurl != ""):
    $image = (@getImageSize($iurl) ? $iurl : base_url()."assets/images/trending.png");
    elseif($s == "" && $iurl == ""):
    $image = base_url()."assets/images/latest.png";
    elseif(isset($news[0]["related"]) && !$trending):
    $image = base_url()."assets/images/archive.png";
    else:
    $image = base_url()."assets/images/trending.png";
    endif;
    return $image;
}
?>