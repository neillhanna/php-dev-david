<div class="container" id="images">
    
    <div class="row col-md-12 fullwidth">
            <form id="formImageSearch" class="form-inline text-center" method="post" action="<?=base_url()?>images">
                <div class="form-group">
                    <label class="control-label" >Sort
                    <select id="sort" class="form-control">
                    		<option>popular</option>
                    		<option>latest</option>
                    	</select></label>
                    <label class="control-label" >Media
                    <select id="media" class="form-control">
                                <option>all</option>
                    		<option>photo</option>
                    		<option>illustration</option>
                    		<option>vector</option>
                    </select></label>
                    <label class="control-label" >Category
                    <select id="category" class="form-control">
                                <option>all</option>
                    		<option>fashion</option>
                    		<option>nature</option>
                    		<option>backgrounds</option>
                    		<option>science</option>
                                <option>education</option>
                    		<option>people</option>
                    		<option>feelings</option>
                    		<option>religion</option>
                    		<option>health</option>
                    		<option>places</option>
                    		<option>animals</option>
                    		<option>industry</option>
                    		<option>food</option>
                    		<option>computer</option>
                    		<option>sports</option>
                    		<option>transportation</option>
                    		<option>travel</option>
                    		<option>buildings</option>
                    		<option>business</option>
                    		<option>music</option>
                    	</select></label>
                    <label class="control-label" id="ec-label" for="editors-choice" >Editors choice
                    <input id="editors-choice" type="checkbox" class="form-control" /><span><span></span></span></label>
                    <input type="text" id="imageSearch" class="form-control" placeholder="Search for Images" value="<?=$s?>"/>
                    <button id="submit" class="form-control btn btn-lg"><span class="glyphicon glyphicon-search"></span></button>
                </div>
            </form>
            <script type="text/javascript">
                
                $(document).ready(function(){
                        $("#sort").val("<?=$sort?>");
                        $("#media").val("<?=$media?>");
                        $("#category").val("<?=$category?>");
                        
                        if(<?=$editor?>){
                            $("#editors-choice")[0].checked = true;
                            $("#ec-label span span").show();
                        }
                });
                
            </script>
    </div>
            
    <main class="row col-md-12 fullwidth gallery">
            <h1 class="page-title"><?php echo $title; ?></h1>

            <?php
                    include("pagination.php");
                    $mod = $feed->totalHits % 20;
                    $tpages = ($feed->totalHits - $mod) / 20;
                    if($mod > 0)
                    {
                        $tpages++;
                    }
            ?>
            
            <?php echo paginate(base_url() . "images/" . $query, $p, $tpages);?>
        
        
            <?php foreach($feed->hits AS $photo): ?>
                
                <div class="row col-lg-3 col-md-4 col-sm-6 image-container">
                    <a class="lightbox fullwidth" href="<?=$photo->webformatURL?>">
                    <img class="center-block" src="<?=$photo->previewURL?>" alt="" /></a>
                </div>
                
            <?php endforeach; ?>
            
    </main>
    <div class="row col-md-12 fullwidth">            
                <div class="row col-md-4"></div>
                <div class="row col-md-4">
                <?php echo paginate(base_url() . "images/" . $query, $p, $tpages); ?>
                <br />
                </div>
                <div class="row col-md-4">
                    <a href="https://pixabay.com/" id="pixabay" class="center-block" style="font-size:10px;line-height:1.1;color:#777;;padding:6px 8px 7.5px;border:1px solid #bbb;width:70px;">
                            powered by
                        <i style="display:block;width:51px;height:12px;overflow:hidden"><img src="<?=base_url()?>assets/images/pixabay.svg" alt="pixabay - royalty free images" style="width:69px"></i>
                    </a>
                </div>
     </div>
            
            
</div>
