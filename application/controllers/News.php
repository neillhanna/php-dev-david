<?php
class News extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model("news_model");
                $this->load->helper("url_helper");
        }

        public function index($s = "", $t = "")
        {
                $s = preg_replace("/%20/"," ",$s);
                if($s == ""){
                    $data["title"] = "Latest News";
                }else{
                    $data["title"] = "News for '" . $s . "'";
                }
                if($t == "trending"){
                    $data["trending"] = true;
                }else{
                    $data["trending"] = false;                
                }
                $s = preg_replace("/ /","+",$s);
                $data["page"] = "News";
                $data["trends"] = $this->news_model->get_trends();
                $data['news'] = $this->news_model->get_news($s);
                $data['s'] = $s;
                $this->load->view('templates/header', $data);
                $this->load->view('news/index', $data);
                $this->load->view('templates/footer');
        }

}
 
