<?php
class Images extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('images_model');
                $this->load->helper('url_helper');
        }

        public function index($s = "", $sort = "popular", $category = "all", $media = "all", $editor = "false", $p = "1")
        {
                $query = $s . "/" . $sort . "/" . $category . "/" .$media . "/" . $editor;
                $data['feed'] = $this->images_model->get_images($s, $sort, $category, $media, $editor, $p);
                $s = preg_replace("/%20/"," ",$s);
                $search = "";
                if($s != " ")
                {
                $search = "for '" . $s . "'";
                }
                $data['title'] = "Royalty free images " . $search;
                $data['page'] = "Images";
                $data["p"] = $p;
                $data["s"] = ($s == " " ? "" : $s);
                $data["query"] = $query;
                $data["sort"] = $sort;
                $data["category"] = $category;
                $data["media"] = $media;
                $data["editor"] = $editor;
                
                $this->load->view('templates/header', $data);
                $this->load->view('images/index', $data);
                $this->load->view('templates/footer');
        }
        
}

