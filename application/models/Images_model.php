<?php
class Images_model extends CI_Model {

        public function __construct()
        {
         
        }
 
        public function get_images($s, $sort, $category, $media, $editor, $p)
        {
                $this->load->spark('curl/1.3.0');
                $this->load->library('curl');
                $this->load->spark('restclient/2.1.0');
                $this->load->library('rest');
                $s = preg_replace("/ /", "+",$s);
                $sort = "&order=" . $sort;
                $category = "&category=" . $category;
                $type = "&image_type=" . $media;
                $editor = "&editors_choice=" . $editor;
                $feed = $this->rest->get("https://pixabay.com/api/?key=1847310-5dcb79991a98d7c3ab1b1b4b8&q=$s&page=$p&safesearch=true" . $sort . $type . $category . $editor, 'application/json');
                return $feed;
        }

        
}

