<?php
class News_model extends CI_Model {

private $key = "2ugEDLhE9MgO2Do8I@JvtLpcbe0_";

        public function __construct()
        {
                $this->load->spark('curl/1.3.0');
                $this->load->library('curl');
                $this->load->spark('restclient/2.1.0');
                $this->load->library('rest');
        }
        
        public function get_news($s)
        {
                $feed = $this->rest->get("http://www.faroo.com/api?q=".$s."&start=1&length=10&rlength=5&l=en&f=json&src=news&i=true&key=".$this->key, "application/json");
                return json_decode(json_encode($feed),true);
        
        }
        

        public function get_trends()
        {
                $feed = $this->rest->get("http://www.faroo.com/api?start=1&l=en&f=json&src=trends&key=".$this->key, 'application/json');
                return json_decode(json_encode($feed),true);
        
        }

        
        public function get_topics($term = "")
        {
                $feed = $this->rest->get("http://www.faroo.com/api?q=".$term."&length=10&rlength=5&l=en&f=json&src=topics&key=".$this->key, "application/json");
                return json_decode(json_encode($feed),true);
        
        }
        
        
        public function get_suggestions($q = "")
        {
                $feed = $this->rest->get("http://www.faroo.com/api?start=1&length=7&l=en&f=json&src=topics&key=".$this->key, 'application/json');
                return json_decode(json_encode($feed),true);
        
        }
        
        
}
 
