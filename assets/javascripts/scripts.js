$(document).ready(function(){

    function base_url(){
        var url = "";
        if(location.host == 'localhost'){
            url = "/php.git/";
        }else{
            url = "/";
        }
        return url;
    }
    
    var base_url = base_url();

    var query = "";
    
    $(".lightbox").magnificPopup({type:"image"});


    $("#formImageSearch").submit(function(){
        query = $("#imageSearch").val() == "" ? "%20" : $("#imageSearch").val();
        query += "/" + $("#sort option:selected").val();
        query += "/" + $("#category option:selected").val();
        query += "/" + $("#media option:selected").val();
        query += "/" + $("#editors-choice").is(":checked");
        
        $("#formImageSearch").attr('action', base_url+"images/" + query);
        
    });
    
    $("#ec-label").click(function(){
        if($("#editors-choice").is(":checked")){
            $("#ec-label span span").show();
        }else{
            $("#ec-label span span").hide();            
        }
    });
       
    $('#topSearchForm').submit(function(e){
        query = $("#topSearch").val() == "" ? "%20" : $("#topSearch").val();
        $("#topSearchForm").attr('action', base_url+"news/" + query);
    });

    $('#sideSearchForm').submit(function(e){
        query = $("#sideSearch").val() == "" ? "%20" : $("#sideSearch").val();
        $("#sideSearchForm").attr('action', base_url+"news/" + query);
    });
    
    $("#sidebar_inner").affix({
        offset: {
            top:-35,
            bottom: function () {
                return (this.bottom = $("footer").outerHeight(true) + 35)
                
            }
        }
    });
    
      
    $('#modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var article = button.data('article');
        var title = button.data('title');
        var modal = $(this);
        modal.find('.modal-title').text(title);
        modal.find('.modal-body').attr("src",article);
    });
    
    $('#modal').on('hidden', function(){
        $(this).data('modal', null);
    });
    
    $('#myModal').on('show.bs.modal', function (event) {
        var img = $(event.relatedTarget);
        var src = img.attr('src');
		var alt = img.attr('alt');
        var urlone = img.data('urlone');
        var urltwo = img.data('urltwo');
        var modal = $(this);
        var carousel_items = "<div class='item active'><img class='img-responsive'  src='"+src+"' alt='"+alt+"'></div>";
        carousel_items += "<div class='item'><img class='img-responsive' src='"+urlone+"' alt='"+alt+"'></div>";
		if(urltwo != ""){
			carousel_items += "<div class='item'><img class='img-responsive'  src='"+urltwo+"' alt='"+alt+"'></div>";
		}
        modal.find('#modalCarousel .carousel-inner').html(carousel_items);
        $('#modalCarousel').carousel({interval:false});
    });
    
});




/*For bootstrap IE 10 support on windows 8 and windows mobile 8*/
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
  var msViewportStyle = document.createElement('style')
  msViewportStyle.appendChild(
    document.createTextNode(
      '@-ms-viewport{width:auto!important}'
    )
  )
  document.querySelector('head').appendChild(msViewportStyle)
}