<?php

function paginate($reload, $page, $tpages) {
	
	$firstlabel = "<span class='glyphicon glyphicon-fast-backward'></span>";
	$prevlabel  = "<span class='glyphicon glyphicon-backward'></span>";
	$nextlabel  = "<span class='glyphicon glyphicon-forward'></span>";
	$lastlabel  = "<span class='glyphicon glyphicon-fast-forward'></span>";
	
	$out = "<div class='text-center fullwidth'>\n";
	
	// first
	if($page>1) {
		$out.= "<a href='" . $reload . "' class='btn btn-primary'>" . $firstlabel . "</a>\n";
	}
	else {
		$out.= "<button class='btn btn-primary' disabled='disabled'>" . $firstlabel . "</button>\n";
	}
	
	// previous
	if($page==1) {
		$out.= "<button class='btn btn-primary' disabled='disabled'>" . $prevlabel . "</button>\n";
	}
	elseif($page==2) {
		$out.= "<a href='" . $reload . "' class='btn btn-primary'>" . $prevlabel . "</a>\n";
	}
	else {
		$out.= "<a href='" . $reload . "/" . ($page-1) . "' class='btn btn-primary'>" . $prevlabel . "</a>\n";
	}
	
	// current
	$out.= "<button class='pag-cur btn btn-default'>" . $page . "/" . $tpages . "</button>\n";
	
	// next
	if($page<$tpages) {
		$out.= "<a href='" . $reload . "/" .($page+1) . "' class='btn btn-primary'>" . $nextlabel . "</a>\n";
	}
	else {
		$out.= "<button class='btn btn-primary' disabled='disabled'>" . $nextlabel . "</button>\n";
	}
	
	// last
	if($page<$tpages) {
		$out.= "<a href='" . $reload . "/" . $tpages . "' class='btn btn-primary'>" . $lastlabel . "</a>\n";
	}
	else {
		$out.= "<button class='btn btn-primary' disabled='disabled'>" . $lastlabel . "</button>\n";
	}
	
	$out.= "</div>";
	
	return $out;
}
?>